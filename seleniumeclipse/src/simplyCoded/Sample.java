package simplyCoded;

interface Try1
{
//All of the below statements are identical.
	//   int a=10;
//   public int a=10;
   public static final int x=10;
   public static final int y=11;
//   final int a=10;
//   static int a=0;
}

interface Try2 extends Try1
{

	public static final int x=20;
	public static final int y = 21;
	
}

class Sample implements Try2
{
  public static void main(String args[])
  {
	  System.out.println(x);
	  System.out.println(y);
	  System.out.println(Try1.x);
	  System.out.println(Try1.y);
	  System.out.println(Try2.x);
	  System.out.println(Try2.y);
   
  }
}