package simplyCoded;
public class EnvironmentProperties {
public static void main(String args[]) {
	System.out.println("file.separator	: "+System.getProperty("file.separator"));
	System.out.println("java.specification.version	: "+System.getProperty("java.specification.version"));
	System.out.println("java.vm.version	: "+System.getProperty("java.vm.version"));
	System.out.println("java.class.path	: "+System.getProperty("java.class.path"));
	System.out.println("java.vendor line.separator	: "+System.getProperty("java.vendor line.separator"));
	System.out.println("java.class.version	: "+System.getProperty("java.class.version"));
	System.out.println("java.vendor.url os.arch	: "+System.getProperty("java.vendor.url os.arch"));
	System.out.println("java.compiler	: "+System.getProperty("java.compiler"));
	System.out.println("java.version	: "+System.getProperty("java.version"));
	System.out.println("os.name	: "+System.getProperty("os.name"));
	System.out.println("java.ext.dirs	: "+System.getProperty("java.ext.dirs"));
	System.out.println("java.vm.name os.version	: "+System.getProperty("java.vm.name os.version"));
	System.out.println("java.home	: "+System.getProperty("java.home"));
	System.out.println("java.vm.specification.name	: "+System.getProperty("java.vm.specification.name"));
	System.out.println("path.separator	: "+System.getProperty("path.separator"));
	System.out.println("java.io.tmpdir	: "+System.getProperty("java.io.tmpdir"));
	System.out.println("java.vm.specification.vendor	: "+System.getProperty("java.vm.specification.vendor"));
	System.out.println("user.dir	: "+System.getProperty("user.dir"));
	System.out.println("java.library.path	: "+System.getProperty("java.library.path"));
	System.out.println("java.vm.specification.version	: "+System.getProperty("java.vm.specification.version"));
	System.out.println("user.home	: "+System.getProperty("user.home"));
	System.out.println("java.specification.name	: "+System.getProperty("java.specification.name"));
	System.out.println("java.vm.vendor user.name	: "+System.getProperty("java.vm.vendor user.name"));
	System.out.println("java.specification.vendor	: "+System.getProperty("java.specification.vendor"));

}
}