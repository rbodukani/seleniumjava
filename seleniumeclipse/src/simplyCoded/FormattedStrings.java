package simplyCoded;

public class FormattedStrings {

	public static void main(String[] args) {
	String name = "Luke";
	int age =32;
	double cost = 8909.9876;
	System.out.println("My name is " + name + ", and I' am " + age + " years old.");
	System.out.printf("My name is %s, and I' am %d years old.",name,age);
	System.out.printf("\nThe cost is $%,.4f",cost);
	System.out.format("\nThe cost is $%,.4f",cost);
	
	String descOfPrice = String.format("%n%s got a 100%% on his exam", "Mike");
	System.out.println(descOfPrice);
	}

}
