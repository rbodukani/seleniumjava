package simplyCoded;

public class PetStore2 {

	public static void main(String[] args){
	//Dog lab = new Dog();
	Dog lab = new Dog("Jimmy","Red",false);
	System.out.println(lab.name);
	System.out.println(lab.color);
	System.out.println(lab.age);
	System.out.println(lab.height);
	System.out.println(lab.male);
	}
	
}

class Dog
{
String name;
String  color;
int age;
double height;
boolean male;

public Dog()
{
	name = "Unknown";
	color = "Unknown";
	age = 0;
	height = 0.5;
	male = true;
}
public Dog(String name,String color,boolean gender)
{
	this.name = name;
	this.color = color;
	//age = 0;
	//height = 0.5;
	male = gender;
}
}