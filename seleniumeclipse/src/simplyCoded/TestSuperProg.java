package simplyCoded;
// Demonstrate char data type.
class TestSuperProg {
public static void main(String args[]) {

	child1 obj = new child1();
	obj.m1();
}
}


class super1{
	int i=10;
	
	void m1()
	{
		System.out.println("Super class constructor");
	}
	
}

class child1 extends super1{
	int i=20;
	child1()
	{
		super();
		System.out.println("Child class constructor");
	}	
	
	void m1()
	{
		System.out.println("Value of i = "+super.i);
		super.m1();
	}
	
}