package simplyCoded;
// Demonstrate char data type.
class TestThisProg {
public static void main(String args[]) {

	Test t = new Test(10,20,30);
	Test x = t;
	t.m1();
	System.out.println(t.equals(x));
}
}


class Test
{
	int a=10;
	void m1()
	{
		System.out.println("The value of a from method m1() " +this.a);
		this.m2();
	}
	void m2()
	{
		System.out.println("The value of a from method m2() " +this.a);
	}
	Test(int x,int y,int z)
	{
		this(x,y);
		System.out.println("The value of z = "+z);
	}
	Test(int x,int y)
	{
		this(x);
		System.out.println("The value of y = "+y);
	}
	Test(int x)
	{
		System.out.println("The value of x = "+x);
	}

}