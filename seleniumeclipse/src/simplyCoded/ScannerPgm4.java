package simplyCoded;
import java.util.Scanner;
public class ScannerPgm4 
{
	public static void main(String[] args)
	{
	@SuppressWarnings("resource")
	Scanner scan = new Scanner(System.in);
	System.out.println("Enter your age: ");
	while(!scan.hasNextInt())
	{
		scan.next();
		System.out.println("Only Integers are allowed. Try again:");
		
	}
	int scanAge = scan.nextInt();
	System.out.println("Your age is : "+scanAge);
	}
}