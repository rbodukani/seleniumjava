package simplyCoded;
import static java.lang.System.out;
public class RelationalOperator {
	public static void main(String[] args) {
		//int a = 12;
		//int b = 24;
		//out.println(a==b);
		String strVar1 = new String("Hello");
		String strVar2 = "Hello";
		String strVar3 = new String("Hello");
		
		//out.println(strVar1 == strVar2);
		//out.println(strVar1 == strVar3);
		
		out.println(strVar1.equals(strVar3));
		out.println(strVar1.equals(strVar2));
	}
}
