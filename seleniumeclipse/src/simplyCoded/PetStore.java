package simplyCoded;

public class PetStore {

	public static void main(String[] args){
	Dog1 lab = new Dog1();
	lab.setName("Barker");
	System.out.println(lab.getName());
	}
	
}


class Dog1
{
final int legs=4;
private String name;
public String  color;
public int age;
public double height;
public boolean male;

public Dog1()
{
	name = "Unknown";
	color = "Unknown";
	age = 0;
	height = 0.5;
	male = true;
}
public Dog1(String name,String color,boolean gender)
{
	this.name = name;
	this.color = color;
	//age = 0;
	//height = 0.5;
	male = gender;
}

public void setName(String name){
	this.name=name;
}
public String getName(){
	return name;
}
}